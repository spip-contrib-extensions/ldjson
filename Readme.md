# Plugin LD+JSON

L'objectif du plugin LD+JSON est de proposer un ensemble de squelettes fournissant des données enrichies au format LD+JSON.


## Installation et activation

Le plugin s'installe comme tout plugin SPIP. Aucune configuration n'est requise. Le plugin active l'inclusion automatique des données enrichies.


## Fonctionnement

Le point d'entrée est le fichier [inclure/ldjson.html](squelettes/inclure/ldjson.html). Il conditionne le chargement des squelettes selon le contexte, selon la présence des objets et des plugins présents.


### Arborescence

Le nommage des fichiers respectent le formalisme suivants :
* [squelettes/ldjson-snippets](squelettes/ldjson-snippets/)/**objet_spip**.html
* [squelettes/ldjson-snippets/plugins/](squelettes/ldjson-snippets/plugins/)**prefixe_plugin**.html

L'appel aux différents blocs se faisant ensuite depuis [inclure/ldjson.html](squelettes/inclure/ldjson.html)

Pour éviter tout conflit de nommage avec les autres pugins et les squelettes, dans le répertoire inclure n'est autorisé que ldjson.html

## Contribuer

Ce plugin est sur la forge collabrative, toute contribution est la bienvenue en ouvrant soit des tickets, soit des branches à volonté.