<?php
/**
 * Injection des ldjson dans les entetes de page
 *
 * @plugin     LdJson
 * @copyright  2022-
 * @author     azerttyu
 * @licence    GNU/GPL
 * @package    SPIP\LdJson\Pipelines
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Effectuer des traitements juste avant l'envoi des pages publiques.
 *
 * => Ajout des métadonnéess LdJSON
 * dans le <head> public de certaines pages.
 *
 * @Note : report du code depuis les plugins Metas+ et cssvar
 * Dont un astuce sale pour connaitre le contexte
 *  
 * @param $flux
 * @return mixed
 */
function ldjson_affichage_final($flux) {

	include_spip('inc/config');
	include_spip('inc/utils'); // pour self()

	$contexte = ldjson_contextualiser_salement();

	// Tests préliminaires avant d'inclure éventuellement les métas
	if (
		// C'est du HTML et on est pas dans le privé
		$GLOBALS['html']
		and !test_espace_prive()
		// Il y a un <head>
		and $pos_head = strpos($flux, '</head>')
		// La page n'est pas en erreur
		and empty($contexte['erreur'])
		// Ce n'est pas une page d'un pseudo fichier (ex. robots.txt.html)
		and !strpos($contexte['type-page'], '.')
	) {

		$fond = "inclure/ldjson";

		if ($ldjson_head = recuperer_fond($fond, $contexte)) {
			$pos_head = strpos($flux, '</head>');
			$flux = substr_replace($flux, $ldjson_head, $pos_head, 0);
		}
	}

	return $flux;
}

function ldjson_contextualiser_salement() {
	include_spip('base/objets');
	$contexte = array(
		'status' => isset($GLOBALS['page']['status']) ? $GLOBALS['page']['status'] : 200,
	);
	
	// Si c'est un objet
	if (isset($GLOBALS['page']['contexte']['type-page']) and $objet = $GLOBALS['page']['contexte']['type-page']) {
		$cle_objet = id_table_objet($objet);
		$id_objet = $GLOBALS['page']['contexte'][$cle_objet] ?? 0;
		$contexte += array(
			'type-page' => $objet,
			'objet' => $objet,
			'id_objet' => $id_objet,
			$cle_objet => $id_objet,
			'cle_objet' => $cle_objet,
		);
	}
	// Sinon c'est peut-être une page=
	elseif (isset($GLOBALS['page']['contexte']['page']) and $page = $GLOBALS['page']['contexte']['page']) {
		$contexte += array(
			'type-page' => $page,
			'page' => $page,
		);
	}
	// Sinon c'est très sûrement la page d'accueil, donc en dur, comme dans le code de assembler
	else {
		$contexte += array(
			'type-page' => 'sommaire',
			'page' => 'sommaire',
		);
	}
	
	return $contexte;
}